import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchInputComponent } from './components/search-input/search-input.component';
import { SearchWidgetComponent } from './containers/search-widget/search-widget.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SearchWidgetComponent, SearchInputComponent],
  imports: [
    CommonModule, HttpClientModule, ReactiveFormsModule, FormsModule
  ],
  exports: [SearchWidgetComponent]
})
export class SearchWidgetModule { }
