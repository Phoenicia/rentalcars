import { Component, OnInit } from '@angular/core';
import { SearchRequestService } from '../../services/search-request/search-request.service';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ifElse, length } from 'ramda';
@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit{
  searchForm: FormGroup;
  searchResults: string[] = [];

  constructor(private searchRequestService: SearchRequestService) { }

  ngOnInit(): void {
    this.searchForm = new FormGroup({
      'search': new FormControl(null)
    });

    this.searchForm.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged()
    ).subscribe((searchTerm: { search: string}) => {
      if (searchTerm.search.length >= 2) {
        this.searchRequestService.getSearchResults(6, searchTerm.search).subscribe((response => {
          this.handleResponse(response);
        }));
      } else {
        this.searchResults = [];
      }
    });
  }

  private handleResponse(response: any) {
    if (this.foundLocation(response)) {
      this.searchResults = response.results.docs.map((location: any) => {
        return location.name;
      });
    } else {
      this.searchResults = ['No results found'];
    }
  }

  private foundLocation(response: any): boolean {
    return response.results.numFound >= 2;
  }
}
