import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchRequestService {
  getSearchResults(numberOfResults: number, search_term: string) {
    const api_url = 'https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=' + numberOfResults +
      '&solrTerm=' + search_term;
    return this.http.get(api_url);
  }
  constructor(private http: HttpClient) { }
}
